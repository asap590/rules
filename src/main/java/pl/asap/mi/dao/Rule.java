package pl.asap.mi.dao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Rule {

    @Id
    @GeneratedValue
    private Long id;

    @Column(length = 255)
    private String domain;

    @Column
    private String url;

    @Column
    private String regexPattern;

    @Column
    @Enumerated(EnumType.STRING)
    private Policy policy;

    @Column
    @Enumerated(EnumType.STRING)
    private RuleType ruleType;
}
