package pl.asap.mi.dao;

public enum Policy {
    PROXY_FORWARD, BLOCK, IGNORE, SEND_FAKE_DATA
}
