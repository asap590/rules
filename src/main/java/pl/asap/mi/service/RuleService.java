package pl.asap.mi.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.asap.mi.dao.Rule;
import pl.asap.mi.dao.RuleRepository;
import pl.asap.mi.dto.RuleDto;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class RuleService {

    private final RuleRepository ruleRepository;

    public List<RuleDto> getAllRules() {
        return ruleRepository.findAll()
                .stream()
                .map(rule -> RuleDto.of(rule.getId(), rule.getDomain(), rule.getUrl(), rule.getRegexPattern(), rule.getPolicy(), rule.getRuleType()))
                .collect(Collectors.toList());
    }

    public void createNewRule(RuleDto ruleDto) {
        Rule rule = Rule.builder()
                .domain(ruleDto.getDomain())
                .url(ruleDto.getUrl())
                .policy(ruleDto.getPolicy())
                .regexPattern(ruleDto.getRegexPattern())
                .ruleType(ruleDto.getRuleType())
                .build();
        ruleRepository.save(rule);
    }
}
