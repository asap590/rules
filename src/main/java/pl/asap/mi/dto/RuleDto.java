package pl.asap.mi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.asap.mi.dao.Policy;
import pl.asap.mi.dao.RuleType;

@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class RuleDto {

    private Long id;

    private String domain;

    private String url;

    private String regexPattern;

    private Policy policy;

    private RuleType ruleType;

}
