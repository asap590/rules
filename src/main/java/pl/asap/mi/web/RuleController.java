package pl.asap.mi.web;

import org.springframework.web.bind.annotation.*;
import pl.asap.mi.dto.RuleDto;
import pl.asap.mi.service.RuleService;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/rules")
public class RuleController {

    private final RuleService ruleService;

    public RuleController(RuleService ruleService) {
        this.ruleService = ruleService;
    }

    @GetMapping
    public List<RuleDto> getRules() {
        return ruleService.getAllRules();
    }

    @PostMapping
    public void addRule(RuleDto ruleDto) {
        ruleService.createNewRule(ruleDto);
    }
}
